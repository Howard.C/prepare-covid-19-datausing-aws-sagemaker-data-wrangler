# Prepare COVID-19 Data Using AWS SageMaker Data Wrangler

## Introduction
Amazon SageMaker Data Wrangler reduces the time it takes to aggregate and prepare data for machine learning (ML) from weeks to minutes. With SageMaker Data Wrangler, you can simplify the process of data preparation and feature engineering, and complete each step of the data preparation workflow, including data selection, cleansing, exploration, and visualization from a single visual interface. 

## Scenario

<p align="center">
    <img src="./images/scenario.png" width="90%" height="90%">
</p>


## Use Case
In this lab, we are going to create a workflow using SageMaker Data Wrangler, First, we will load a data file from S3 Bucket. Next, extract features from our data. Final, export a production-ready data preparation workflow on a Jupyter Notebook.

## Preparation

- Done with opening a SageMaker Studio.
    - [Onboard Quickly to Amazon SageMaker Studio ](https://www.youtube.com/watch?v=wiDHCWVrjCU&ab_channel=AmazonWebServices)
- Upload these files in your S3 Bucket, and make sure S3 bucket have the right access policy.
    - [Policy](https://docs.aws.amazon.com/sagemaker/latest/dg/data-wrangler-security.html#data-wrangler-security-iam-policy)    
    - [Accu_cases.csv](./materials/Accu_cases.csv)
    - [Daily_cases.csv](./materials/Daily_cases.csv)


## Lab tutorial
### ☀ Import dataset
- When Studio opens, click **New** in the file and click **Flow**. This creates a new folder in Studio with a .flow file inside, which contains your data flow. The .flow file automatically opens in Studio.

<p align="center">
    <img src="./images/flow_create.png" width="90%" height="90%">
</p>

- (Optional) Rename the new folder and the .flow file.
- When you create a new .flow file in Studio, you may see a message : "**Connecting to engine Establishing connection to engine...**"
This can take around 5 minutes the first time you launch Data Wrangler.
- When it complete connecting, choose a data source and use it to import a dataset. In this case, we choose **S3**. 

<p align="center">
    <img src="./images/choose_data_source.png" width="90%" height="90%">
</p>

- Select **Amazon S3**.
- Select your S3 Bucket and click **import dataset** that you have uploaded. 
Click **import** to go back import data page and do it again to finish import different data for S3 Bucket.

<p align="center">
    <img src="./images/data_import.png" width="90%" height="90%">
</p>

#### ☀ Join data 

- Click **+** and click **Join**.

<p align="center">
    <img src="./images/join_data.png" width="90%" height="90%">
</p>

- Click right data that you wanted to join, you will see  a line generated and pointed to a new dataset. Now click **Configure**.
- In Join Configure, select the join type **Left outer**, and select **Date** for left and right to join and click **Apply**.

<p align="center">
    <img src="./images/join_apply.png" width="90%" height="90%">
</p>

#### ☀ Transform data 

- Click **+** and click **Add transform** and you will see the following page.

<p align="center">
    <img src="./images/gotransform.png" width="90%" height="90%">
</p>

<p align="center">
    <img src="./images/transform.png" width="90%" height="90%">
</p>


- Click **Custom Transfrom** and choose Python(Pandas), input the code as below. Click **Preview** to see the result. Then click **Add**.

```
import numpy as np
df["Accu_cases_lag1"] = df['Accu_cases'].shift(1)
df["Accu_cases_lag2"] = df['Accu_cases'].shift(2)
df["Accu_cases_lag3"] = df['Accu_cases'].shift(3)
df["Accu_cases_lag4"] = df['Accu_cases'].shift(4)
df['trend'] = np.arange(len(df))
df['log_trend'] = np.log1p(np.arange(len(df)))
df['sq_trend'] = np.arange(len(df)) ** 2
```
- Now we click **Handle missing**. 
    - On Transform selection, choose **Drop missing**. 
    - On Dimension selection, choose **Drop Rows**. 
    - On Input column, choose **Accu_cases_lag4**.
    - Click **Preview** to see the result. Then click **Add**.
- Now click **Analyze**  for the next step.

#### ☀ Estimate performance of a model on our dataset

- On Analysis type, click  **+** -> click **Add analysis**. 
Choose **Quick model**, on Label selection, choose **Accu_cases**.
> Use the Quick Model visualization to quickly evaluate your data and produce importance scores for each feature.
- Click **Create**. You will see the correlation with your column.

<p align="center">
    <img src="./images/quick_model.png" width="90%" height="90%">
</p>

- Now you can evaluate your data and consider to modify your data or not. 
> A feature importance score score indicates how useful a feature is at predicting a target label. The feature importance score is between [0, 1] and a higher number indicates that the feature is more important to the whole dataset. 

#### ☀ Export your dataflow to SageMaker Notebook 
When you create a Data Wrangler data flow, you can choose from four export options to easily integrate that data flow into your data processing pipeline. Data Wrangler offers export options to SageMaker Data Wrangler Job, Pipeline, Python code and Feature Store.
**To export your Data Wrangler flow:**
- Click **Export**.
- Click the final table for export.

<p align="center">
    <img src="./images/export_data.png" width="90%" height="90%">
</p>

- Click the last one modify record then click **Export Step** on the right hand.
- Click **Data Wrangler Job** for export.
- Choose **Python 3 (Data Science)** for the Kernel and run it directly in Studio to execute your data flow and process your data.

<p align="center">
    <img src="./images/notebook.png" width="90%" height="90%">
</p>

## Conclusion
Congratulations! You now have learned how to:
- You can integrate a Data Wrangler data flow into your machine learning (ML) workflows with few or no code.
- You have learn how to import, transform, analysis and export to a SageMaker Notebook.

## Clean Up
When you are not using Data Wrangler, it is important to shut down the instance on which it runs to avoid incurring additional fees. 

__To shut down the Data Wrangler instance in Studio:__

- In Studio, select the Running Instances and Kernels icon 
- Under RUNNING APPS is the sagemaker-data-wrangler-1.0 app. Select the shut down icon next to this app.

## Reference
* [Amazon SageMaker Developer Guide](https://github.com/guodongxiaren/README)








